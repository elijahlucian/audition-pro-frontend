import React, { Component } from 'react'
import ReactMic from 'react-mic'

class AuditionRecorder extends Component {

    constructor(props) {
        super(props)
        console.log(props);
        
        this.state = {
            line: "null",
            constraints: {audio: true},
            record: false,
            audioClips: [<div>hello</div>]
        }

        // this.audioClips = React.createRef()
        this.audio = React.createRef()
        this.btnRecord = React.createRef()
        this.btnStop = React.createRef()
        this.recording = React.createRef()
    }

    // var constraints = { audio: true }
    
    captureUserMedia = (constraints, successCallback) => {
        navigator.mediaDevices.getUserMedia(constraints)
        .then(successCallback)
        .catch((err) => {
            console.log("ERROR:", err.name, ":",err.message)
        })
    }

    onMediaSuccess = (stream)  => {
        console.log("Getting media stream", stream)
        var mediaRecorder = new MediaRecorder(stream)
        
        this.btnRecord.onclick = (blob) => {
            this.setState({record: true})
            mediaRecorder.start()
        }

        this.btnStop.onclick = (blob) => {
            this.setState({record: false})

            mediaRecorder.stop()
            
            let containerThing = <div>woot!</div>

            this.setState({
                audioClips: [...this.state.audioClips, containerThing]
            })
            
        }

    }

    saveTake = () => {
        // might need to stop recording
        // mediaRecorder.save()

        if (this.props.lineNumber < this.props.currentPart.lines.length - 1) {
            console.log(this.props.lineNumber, this.props.currentPart.lines.length, "save recorded file, line.filename, advance the script")
            this.props.advanceLine()
        } else { 
            console.log("you have reached the end of the lines")
        }
    }

    componentDidMount() {
        
        let currentLines = this.props.currentPart.lines
        this.setState({
            line: currentLines[this.props.lineNumber]
        })
        this.captureUserMedia(this.state.constraints, this.onMediaSuccess)     
    }


    render() {

        return(
            <div>
                <h1>Audio Recorder</h1>
                <h3>{this.props.currentPart.characterName}</h3>
                <p>Profile: {this.props.currentPart.characterProfile}</p>
                <p>Voice Type: {this.props.currentPart.voiceType}</p>            
                <div>
                    <ReactMic />
                    <h3>Record</h3>
                    <p>AUDIO</p>
                    <audio ref={this.audio} source={this.recording} controls='true'/>
                    <article ref="audioClips" >
                        {this.state.audioClips}
                    </article>
                    <p>HERE</p>
                    <div className="preview" onClick={this.playAudition}>
                        <p>Nifty WAV preview box</p>
                        <p>shift-click in here to set start and end points of wav</p>
                        <p>left-click to hear your recording back</p>
                        <p>add fast-mode button</p>
                    </div>
                    <p className="line">{this.state.line.text}</p>
                    <p>context: {this.state.line.context}</p>
                    <div className="controls">
                        <a onClick={this.record} ref={(node) => {this.btnRecord = node}}>record</a>
                        <a onClick={this.reRecord}>re-record</a>
                        <a onClick={this.stop} ref={(node) => {this.btnStop = node}}>stop</a>
                        <a onClick={this.reTake}>re take</a>
                        <a onClick={this.altTake}>alt take</a>
                        <a onClick={this.saveTake}>save take</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default AuditionRecorder
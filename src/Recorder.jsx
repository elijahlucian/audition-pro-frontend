
import React from 'react'
import { ReactMic } from 'react-mic';
import { RecorderState } from './RecorderState'


export class Recorder extends React.Component {
  constructor(props) {
    super(props);
    // props are {current_part, take_number, advance_line()}
    this.state = RecorderState
  }
  
  componentDidUpdate() {}
  
  componentDidMount() {
    var form_data = new FormData()
    form_data.append('part_id',this.props.current_part.id)
    
    let current_lines = this.props.current_part.lines
    this.setState({
        line: current_lines[this.props.take_number],
        lines: current_lines,
        form_data: form_data
    })
  }
  
  save_recording = () => {
    
    let form_data = this.state.form_data
    
    form_data.append('blob_objects[]', this.state.current_blob.blob)
    form_data.append('line_ids[]', this.props.current_part.lines[this.props.line_number].id)
    form_data.append('take_indexes[]', this.state.take)    
    let audition_blobs = [...this.state.audition_blobs, this.state.current_blob]
    this.setState({ 
      audition_blobs: audition_blobs,
      form_data: form_data
    })

  }

  save_audition = () => {
    let form_data = this.state.form_data
    form_data.append('part_id', this.props.current_part.id)    
    this.props.save_audition(form_data)

  }

  start_recording = () => {
    this.setState({
      record: true,
      recording_state: 'recording',
      scope_background: '#aa3434',
      scope_foreground: '#eee',
      recording_class: 'recording'
    });

  }

  stop_recording = () => {
    this.setState({
      record: false,
      recording_state: 'after',
      scope_background: '#ccc',
      scope_foreground: '#444'
      });
  }

  onData(blob) {
  }

  onStop = (blob) => {
    
    let key = `${this.props.line_number}_${this.state.take}`
    let recorded_audio = (
      <div className="audition take" key={key}>
    <h3>{this.props.current_part.character_name} - Line {this.props.line_number + 1} - Take {this.state.take}</h3>
    <audio src={blob.blobURL} controls='true' />
    </div>
    )

    this.setState({
      current_take_audio: recorded_audio,
      current_blob: blob
    })
  }


  restart_recording = () => {
    this.setState({record: false}, ()=>{this.start_recording()})
    console.log('restarting recording');
  }

  restart_take = () => {
      console.log('clear buffer and start recording');
  }

  save_and_new_take = () => {
      console.log('save and stay on line');
      this.stop_recording()
      this.save_recording()
      this.setState({take: this.state.take + 1})
      this.start_recording()
  }

  save_and_next_line = () => {
      
      let new_state

      this.save_recording()

      const base_state = {
        recording_state: 'stopped',
        current_take_audio: '',
        current_blob: {}
      }

      if(this.state.lines.length - 1 > this.props.line_number){
        this.props.advance_line()
        new_state = {
          ...base_state,
          take: 1
        }
      } else {
        new_state = {
          ...base_state,
          take: this.state.take + 1,
          alert: "you have reached the end of the lines",
          completed: true
        }
      }

      this.setState(new_state)

  }

  render() {

    let controls;
    let finalize_button;

    if (this.state.completed) {
      finalize_button = (
        <div className="submit" onClick={this.save_audition}>Review All Takes</div>)
    } else {
      finalize_button = ''
    }

    switch (this.state.recording_state) {
      case 'stopped':
        controls = (
          <div className="controls">
            <p className="alert">start recording</p>
            <a onClick={this.start_recording}>record</a>
          </div>
        )
        break
      case 'recording':
        controls = (
          <div className="controls">
            <p className="alert">stop or restart the recording</p>
            <a onClick={this.stop_recording}>stop</a>
            <a onClick={this.restart_recording}>restart</a>
          </div>
        )
        break
      case 'after':
        controls = (
          <div className="controls">
            <p className="alert">save and move to next line or</p>
            <a onClick={this.save_and_next_line}>Next Line</a>
            <a onClick={this.save_and_new_take}>Alternate Take</a>
            <a onClick={this.restart_recording}>Discard Recording</a>
          </div>
        )
        break
      default:
        break
    }

    return (
      <div>
        <div>
            <p className="alert">{this.state.alert}</p>
            <h3>Audition Recorder</h3>
            <p>{this.props.current_part.character_name}</p>
            <h4>Line #{this.props.line_number + 1} - Take #{this.state.take}</h4>
            <p className="prompt">"{this.state.lines[this.props.line_number].text}"</p>
            <p>Context: {this.state.lines[this.props.line_number].context}</p>
        </div>

        <div className={this.state.recording_state}>
        <ReactMic
        record={this.state.record}
          className="sound-wave"
          onStop={this.onStop}
          onData={this.onData}
          strokeColor={this.state.scope_foreground}
          backgroundColor={this.state.scope_background} 
        />
        </div>

        {finalize_button}
        {controls}
        {this.state.current_take_audio}
      </div>
    );
  }
}

export default Recorder
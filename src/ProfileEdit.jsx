import React from 'react'

const ProfileEdit = ({kind}) => {
    return(
        <div>
            <h3>{kind} (edit/new) ProfileEdit</h3>
            <p className="note">kind is either 'actor' or 'creator'</p>
        </div>
    )
}

export default ProfileEdit
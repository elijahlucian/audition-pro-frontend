import React from 'react'

const AuditionEdit = ({project_id, kind}) => {
    
    let counter = 0;

    const validateFields = (e) => {
        counter += 1;
        console.log(e.target, counter)
    }

    const handleClick = (e) => {
        console.log("form submitted");
        // submit object with parent component function newAudition()

        e.preventDefault()
    }
    
    return (
        <div>
            <h3>(edit/new) Audition</h3>
            <form onChange={validateFields} onClick={handleClick} id="auditionForm">
                
                <h3> choose project </h3>
                <label name="projectName">Project: (selectbox)</label>
                
                <h3>Create audition for character</h3>
                <label name="characterName">Character Name: <input /></label>
                <label name="profile">Character Profile: (short descriptive text about character)<input /></label>
                <label name="description">Long Character Description<input /></label>
                <label name="voiceType">(tag select)</label>
                <label name="age">(number)<input /></label>
                <label name="accent">(choose from list) - 'not in list?'</label>
                <label name="characterReference">(name from production) or link to video</label>
                
                <button>save</button><button>save and new</button>
            </form>
            <p className="note">add cool typing animation to the form onMouseOver. describe what the user should enter.</p>
            <p className="note">can C/U project and audition be taken from the same component?></p>
            <p className="note">kind is create or edit. possibly destroy</p>
        </div>
    )
}

export default AuditionEdit
export const State = {

    logged_in_user: {
      id: "33a2f789-f7e1-40b2-aa6f-e47774fae01f",
      username: '',
      kind: '',
      auditions: []
    },


    current_audition: {
      part_id: null,
      id: null,
      submitted: false,
      created_at: null,
      audio_takes: [
        {
          comments: [
            {
              body: '',
              author: ''
            }
          ]
        }
      ]
    },
    
    projects: [],
    
    users: [
      {
        id: null,
        username: 'nullzor',
        email: null        
      }
    ],
    
    line_number: 0,
    
    current_part: {
      id: '6edf05d4-22d3-42f6-9e04-95783f049e8d',
      character_name: '',
      lines: [
        {
          text: "No Part Selected!",
          context: "No Part Selected!",
          filename: "line.wav"
        }
      ]
    }
  }
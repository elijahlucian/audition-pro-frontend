import React from 'react'

const LogIn = (props) => {
    
    // console.log(props.users);
    
    let users = props.users.map((actor,i) =>
        <div key={i} onClick={(e) => {props.handleLogin(e, actor)}}>{actor.username} - {actor.kind}</div>
    )
    
    return(
        <div>
            <h1>Log In</h1>
            {users}      
        </div>
    )
}

export default LogIn
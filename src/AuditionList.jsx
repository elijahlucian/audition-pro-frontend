import React from 'react'

const AuditionList = ({projects, set_current_part}) => {
    
    let open_auditions = projects.map((project,i) => 
        <div key={i}>
            <h3>{project.name}</h3>
            <p>{project.description}</p>
            <h3>Parts</h3>
            {project.parts.map((part, j) =>
                <div className="part" key={j}>
                    <h3>{part.character_name}</h3>
                    <p>{part.character_profile}</p>
                    <p>{part.voice_type}</p>
                    <p>Age: {part.age}</p>
                    <div className="button" onClick={(e) => {set_current_part(e, {project: i, part: j})}}>Audition For This Character</div>
                </div>
            )}
        </div>
    )

    return(
        <div>
            <h1>Open Auditions</h1>
            <div className="auditions">
                {open_auditions}
            </div>
        </div>
    )
}

export default AuditionList
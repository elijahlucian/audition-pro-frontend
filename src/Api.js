import { API_ROOT } from './App'
import axios from 'axios'

export const post_data = (endpoint, body = {}) => {
    console.log("POST", endpoint, body);
    
    return axios({
        method: 'post',
        url: `${API_ROOT}/${endpoint}`,
        data: body
    })
}

export const get_data = (endpoint) => {
    console.log("GET", endpoint);
    
    return axios(`${API_ROOT}/${endpoint}`)
    .catch(err => console.log(err))
}

//////////////////////// HELP /////////////////////////////

const build_endpoint = (endpoint_and_params) => {
    return endpoint_and_params.join('/')
}

////////////////////////  GETS  ///////////////////////////

export const get_audition = (user_id, part_id) => {
    return get_data(build_endpoint( ['audition', user_id, part_id] ))
}

export const user_info = (user_id) => {
    return get_data(build_endpoint( ["user", user_id] ))
}

export const get_all_users = () => {
    return get_data("users")
}

export const get_comments = (user_id, audition_id) => {
    return get_data(build_endpoint( [ "comments", user_id, audition_id ] ))
}

export const project_collection = () => {
    return get_data("projects")
}

export const project_single = (project_id) => {
    return get_data(build_endpoint( ["projects",  project_id] ))
}

export const parts_collection = (project_id) => {
    return get_data(build_endpoint( ["parts",  project_id] ))
}

export const lines_for_part = (part_id) => {
    return get_data(build_endpoint( ["lines", part_id] ))
}


////////////////////////  POSTS  ///////////////////////////

export const submit_audition = (line_id, user_id) => {
    build_endpoint( [line_id, user_id] )
    // actors submit auditions for parts
}

export const save_audition = (user_id, part_id, form) => {
    return post_data(build_endpoint( ['audition', user_id, part_id] ), form)
}

export const submit_part = (project_id, user_id) => {
    build_endpoint( [project_id, user_id] )
    // creators submit part
}

export const submit_project = (user_id) => {
    // creators submit project here
}

export const submit_comment = (user_id, audio_take_id, comment_body) => {
    return post_data(build_endpoint( ['comments', user_id, audio_take_id] ), comment_body )
    // creators and actors can comment on auditions
}
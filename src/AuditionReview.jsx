import React from 'react'

export class AuditionReview extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            comment_body: '',
            changed: false,
        }
    }
    
    componentDidMount() {
        this.props.get_current_audition()
        // get 'audition/:part_id' 
        // logged in user will do the thing,
    }

    componentDidUpdate() {console.log(this.props.current_audition.audio_takes)}

    submit_audition = () => {}

    get_line = (line_id) => {
        return this.props.curent_part.lines.find((line) => {
            return line.id === line_id
        })
    }

    add_comment = (e, audio_take_id, audio_take_index) => {
        this.props.add_comment(audio_take_id, this.state.comment_body, audio_take_index)
        e.preventDefault();
        e.target.reset()
        // we need
            // audio_take_id
            // delegate logged in user to parent state
    }

    update_comment_state = (e) => {
        console.log("PING!", e.target.value);
        let comment_body = e.target.value
        this.setState({
            comment_body: comment_body
        })
    }

    render() {
        
        let current_part = this.props.current_part
        let lines = this.props.current_part.lines
        let current_audition = this.props.current_audition.audio_takes.map((item, i) => {
            return (
                <div className="review" key={i}>
                    <div className="left">
                        {/* <h3>{this.get_line(item.line_id)}</h3> */}
                        <div className="audition-box">
                            <div className="tabs">
                                <div>1</div>
                                <div>2</div>
                                <div>+</div>
                            </div>
                            <div className="innards">
                                <audio src={`api/media/${item.id}`} controls />
                                <p>waveform editor</p>
                                <p>add extra takes</p>
                                <p>record in box</p>
                            </div>
                        </div>
                    </div>
                    <div className="right">
                        <h3>Tools</h3>
                        <div className="toolbox">
                            <div className="tabs">
                                
                                <div>details</div>
                                <div></div>
                                <div>comments</div>
                            </div>
                            <div className="innards">
                            <h4>comments</h4>
                            {item.comments.slice(-5).map((comment, j) => (
                                     <div key={j}> {comment.author} - {comment.body}  </div> 
                                    )
                                )}
                                <form autocomplete="off" className="new_comment" onSubmit={(e) => this.add_comment(e, item.id, i)}>
                                    <input onChange={this.update_comment_state} name="comment_body" />
                                    <button>send message</button>
                                </form>
                                <p>line and context</p>
                                <p>visual references</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                
            )
        })

        return (
            <div>
                <div>TODO list of user's auditions</div>
                <h3>Your Audition (sprint 2)</h3>
                {current_audition}
                
            </div>
        )
    }
}

export default AuditionReview
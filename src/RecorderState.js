// RecorderState.js

export const RecorderState = {
    // GUI State
    record: false, // this tells the recorder to go and stop.
    completed: false, // at least one saved take per line
    endOfScript: false,
    recording_class: '',

    // GUI display
    alert: '', // gui message
    take: 1, // counted from number of saved takes
    
    scope_foreground: '#444',
    scope_background: '#ccc',

    line: "null",
    lines: [
      {
        text: null,
        context: null
      }
    ],
    
    // controls state
    recording_state: 'stopped',

    // current take
    current_take_audio: '',
    current_blob: {},

    // Package State
    form_data: null,
    audio_files: [],
    audition_blobs: []
}

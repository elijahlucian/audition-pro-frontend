import React, { Component } from 'react';
import LogIn from './LogIn'
import ActorDashboard from './ActorDashboard';
import ProfileEdit from './ProfileEdit'
import Project from './Project'
import AuditionEdit from './AuditionEdit'
import AuditionList from "./AuditionList";
import CreatorDashboard from "./CreatorDashboard";
import AuditionReview from './AuditionReview'
import Recorder from './Recorder'
import { State } from './State'
import './App.css';
import * as Api from './Api.js'
import { Link, Route, withRouter } from 'react-router-dom'
import { comment } from 'postcss';

export const API_ROOT = 'api'

class App extends Component {
  state = State
  
  notes = () => {
    // start with login,
    // on selection, move to open auditions
    // flow is good until "save audition"
    // 
  }

  componentDidMount() {
    Api.user_info(this.state.logged_in_user.id).then(response => {
      this.setState({
        logged_in_user: {
          id: response.data.id,
          username: response.data.username
        }
      })
    })
    Api.project_collection().then(response => {
      
      
      this.setState({
        projects: response.data
      })
    })
    Api.get_all_users().then(response => {
      this.setState({
        users: response.data
      })
    })
    this.change_page('/login')
  }

  componentDidUpdate() {
    console.log(this.state);
  }

  ///////////////////////////////////////NAVICATION/////////////////

  change_page = (url) => {
    this.props.history.push(url)
  }

  menuClick = (e) => {
    let current_page = e.currentTarget.dataset.id
    this.setState({current_page: current_page})
    e.preventDefault()
  }

  ///////////////////////////////////////STATE////////////////////////////

  set_audition_state = (auditions, current_part) => {
    // console.log("ASDFDSAFADFSA", auditions, current_part);
    
    if ( auditions.find((a) => {return a.part_id === current_part.id}) ) {
      
      console.log("GO TO AUDITON MAKER THINGY");
      this.get_current_audition()
      this.change_page('audition_review')
      
    } else {
      
      console.log(" GO TO AUDIO RECORDER THINGY!!!! ");
      this.setState({
        current_audition: {
          part_id: null,
          id: null,
          submitted: false,
          created_at: null,
          audio_takes: []
        },
      })
      this.change_page('/recorder')
    }
  }

  set_current_part = (e, {project, part}) => {
    // this will be a database call.
    
    let auditions = this.state.logged_in_user.auditions
    let current_part = {
      project_name: this.state.projects[project].name,
      ...this.state.projects[project].parts[part]
    }
    // check this.state.currentAudition to see if audition is empty
    this.setState({
      current_part: current_part,
      line_number: 0
    }, () => {this.set_audition_state(auditions, current_part)})
     
    e.preventDefault()
  }
  
  advance_line = () => {
    console.log("ping");
    this.setState({line_number: this.state.line_number + 1})
  }
  
  ////////////////////////////////////////////////AUDITONS //////////////////////////

  get_current_audition = () => {
    let params = {
      user_id: this.state.logged_in_user.id,
      part_id: this.state.current_part.id
    }
    // console.log(params);
    
    Api.get_audition(params.user_id, params.part_id)
    .then(response => {
      this.setState({
        current_audition: response.data
      })
    })
  }

  save_audition = (audition) => {
    Api.save_audition(this.state.logged_in_user.id, this.state.current_part.id, audition)
    .then(response => {
      this.setState({
        current_audition: response.data
      })
    })
    this.change_page('/audition_review')
  }

  add_comment = (audio_take_id, body, audio_takes_index) => {
    let user_id = this.state.logged_in_user.id
    let comment_body = { body: body }

    let state = this.state

    state = {
      
    }

    // console.log(user_id,audio_take_id,comment_body);    
    Api.submit_comment(user_id, audio_take_id, comment_body)
    .then(response => { 
      this.setState({
        current_audition: response.data
      })
    })
  }

  update_audition = () => {}
  submit_audition = () => {}
  destroy_audition = () => {}

  ////////////////////////////////////////////LOGIN//////////////////////////////

  handleLogin = (e, username) => {
    Api.user_info(username.id).then(response => {
      this.setState({
        logged_in_user: {
          id: response.data.id,
          username: response.data.username,
          kind: response.data.kind,
          auditions: response.data.auditions
        }
      }, () => {
        if (this.state.logged_in_user.kind === "Actor") {
          this.change_page('/auditions')
        } else if (this.state.logged_in_user.kind === "Creator") {
          this.change_page('/creator')
        } 
      }
    )
    })
  }

  newProject = () => {
    console.log("new project created!");
    // take user to new audition dialog, with new project selected. 
    // "now let's get some auditions for your project"
  }
  newAudition = () => {
    console.log("new audition created!");
    // add mailer call on backend that pings all active actors.
    
  }

  render() {

    let menu;

    switch (this.state.logged_in_user.kind) {
      case 'Actor':
        menu = (
        <div className="menu">
          <Link to="/login">Login</Link>
          <Link to="/auditions">Open Auditions</Link>
          <Link to="/dashboard">Your Dashboard</Link>
          <Link to="/audition_review">Audition Review</Link> 
          <Link to="/recorder">Recorder</Link> 
        </div>
        )
        break;
    
      case 'Creator':
      menu = (
        <div className="menu">
          <Link to="/login">Login</Link>
          <Link to="/new_project" params={{}}>Add Project</Link>
          <Link to="/dashboard">Your Dashboard</Link>
          <Link to="/audition_review">Audition Review</Link> 
        </div>
        )
        break;
    
      default:
      menu = (
        <div className="menu">
          <Link to="/login">Login</Link>
        </div>
        )
        break;
    }

    return (
      <div className="App">
        <header className="App-header">
          <h1>AuditionPro</h1>
          {menu}
        </header>
        <div className="main_content">

        <h3>User: {this.state.logged_in_user.username}</h3>
        <h3>Project: {this.state.current_part.project_name }</h3>
        <h3>Part: {this.state.current_part.character_name}</h3>
        

          <Route path="/login" render={(props) => (
            <LogIn 
              users={this.state.users} 
              handleLogin={this.handleLogin} />
          )}/>

          {/* Stateless Components */}
          
          <Route path="/dashboard" component={ActorDashboard} />
          <Route path="/profile" component={ProfileEdit} />
          <Route path="/creator" component={CreatorDashboard} />
          <Route path="/creator-editor" component={AuditionEdit} />

          {/* Class Components */}

          <Route path="new_project" render={(props) => (
            <Project 
              form_kind={'new'}
            />
          )}/>

          <Route path="/audition_review" render={(props) => (
            <AuditionReview
              add_comment={this.add_comment}
              current_part={this.state.current_part}
              get_current_audition={this.get_current_audition}
              current_audition={this.state.current_audition}
            />
          )}/>
          
          <Route path="/recorder" render={(props) => (
            <Recorder 
              save_audition={this.save_audition}
              // upload_audition={this.upload_audition}
              change_page={this.change_page}
              current_part={this.state.current_part} 
              line_number={this.state.line_number}
              advance_line={this.advance_line}
              review_audition={this.review_audition}
              logged_in_user={this.state.logged_in_user} />
          )}/>
          
          <Route path="/auditions" render={(props) => (
            <AuditionList 
              projects={this.state.projects} 
              set_current_part={this.set_current_part} />
          )}/>
        
        </div>
      </div>
    );
  }
}

export default withRouter(App);

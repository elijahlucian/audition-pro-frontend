import React from 'react'

export class Project extends React.Component {
    
    constructor(props) {
      super(props);
      // props are {current_part, take_number, advance_line()}
      this.state = {}
    }

    validateForm = () => {}
    submitForm = () => {} // decide to trigger new or edit based on form_kind

    render() {
    
        return(
            <div>
                <h1>{this.props.form_kind}(edit/new) Project</h1>
                <p></p>
            </div>
        )
    }
}

export default Project
